package com.moderocky.mask.annotation;

import java.lang.annotation.*;

@API
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Configurable {

    String value() default "";

    boolean override() default false;

    @API
    @Inherited
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.FIELD)
    @interface Bounded {

        double maxValue();

        double minValue();

    }

    @API
    @Inherited
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.FIELD)
    @interface Keyed {

        String value();

    }

    @API
    @Inherited
    @Retention(RetentionPolicy.RUNTIME)
    @Target({ElementType.FIELD})
    @interface Regex {

        String matcher();

        String alternative();

        //Class<? extends StringChecker> checker() default StringChecker.class;

    }

    @API
    @Inherited
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.FIELD)
    @interface Serialise {

        boolean stringify() default true;

        String method();

    }

    @API
    @Inherited
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.FIELD)
    @interface Comment {

        String[] value();

    }

    @API
    @Inherited
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.TYPE)
    @interface Header {

        String[] value();

    }

    @API
    @Inherited
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.TYPE)
    @interface Overwrite {

        boolean value() default true;

    }

}
