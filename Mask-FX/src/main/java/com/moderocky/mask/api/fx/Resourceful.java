package com.moderocky.mask.api.fx;

import java.io.*;
import java.nio.charset.StandardCharsets;

public interface Resourceful {

    default String getResource(String key) throws RuntimeException {
        InputStream stream = this.getClass().getClassLoader().getResourceAsStream(key);
        if (stream == null) return null;
        StringBuilder buffer = new StringBuilder();
        try (Reader reader = new BufferedReader(new InputStreamReader(stream, StandardCharsets.UTF_8))) {
            int c;
            while ((c = reader.read()) != -1) {
                buffer.append((char) c);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return buffer.toString();
    }

    default byte[] getResourceBytes(String key) throws RuntimeException {
        InputStream stream = this.getClass().getClassLoader().getResourceAsStream(key);
        if (stream == null) return new byte[0];
        try {
            byte[] bytes = new byte[stream.available()];
            stream.read(bytes);
            return bytes;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
