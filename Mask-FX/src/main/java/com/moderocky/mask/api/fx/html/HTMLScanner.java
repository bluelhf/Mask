package com.moderocky.mask.api.fx.html;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.moderocky.mask.api.MagicMap;
import com.moderocky.mask.api.MagicStringList;
import com.moderocky.mask.api.fx.Resourceful;

import java.io.File;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HTMLScanner implements Resourceful {
    public static final Pattern PATTERN = Pattern.compile("json:\\/\\/([a-zA-Z0-9_-]+(?:\\/[a-zA-Z0-9_-]+)*)");
    public static final Pattern ARRAY = Pattern.compile("array:\\/\\/([a-zA-Z0-9_-]+(?:\\/[a-zA-Z0-9_-]+)*)?\\?([a-zA-Z0-9_-]+\\.html)");
    final JsonObject object;
    final String content;

    public HTMLScanner(JsonObject data, String template) {
        this.object = data;
        this.content = template;
    }

    public String create() {
        String string = content;
        string = matchArrayTags(string, object);
        for (String tag : getJsonTags(string)) {
            string = string.replace("json://" + tag, retrieve(tag, object));
        }
        return string;
    }

    private String replaceJson(String string, JsonObject object) {
        for (String tag : getJsonTags(string)) {
            string = string.replace("json://" + tag, retrieve(tag, object));
        }
        return string;
    }

    private MagicStringList getJsonTags(String string) {
        MagicStringList list = new MagicStringList();
        Matcher matcher = PATTERN.matcher(string);
        while (matcher.find()) {
            list.add(matcher.group(1));
        }
        return list;
    }

    private String matchArrayTags(String string, JsonObject object) {
        MagicMap<String, String> map = new MagicMap<>();
        Matcher matcher = ARRAY.matcher(string);
        while (matcher.find()) {
            map.put(matcher.group(1), matcher.group(2));
        }
        for (Map.Entry<String, String> entry : map.entrySet()) {
            StringBuilder builder = new StringBuilder();
            String html = getResource(entry.getValue());
            JsonArray array = getElement(entry.getKey(), object).getAsJsonArray();
            if (array == null) {
                string = string.replace("array://" + entry.getKey() + "?" + entry.getValue(), "N/A");
                break;
            }
            for (JsonElement element : array) {
                builder.append(replaceJson(html, element.getAsJsonObject()));
            }
            string = string.replace("array://" + entry.getKey() + "?" + entry.getValue(), builder.toString());
        }
        return string;
    }

    private JsonElement getElement(String key, JsonObject object) {
        MagicStringList list = new MagicStringList(key.split("\\/"));
        JsonElement current = object;
        for (String string : list) {
            if (current instanceof JsonObject) current = ((JsonObject) current).get(string);
        }
        return current;
    }

    private String retrieve(String key, JsonObject object) {
        MagicStringList list = new MagicStringList(key.split("\\/"));
        JsonElement current = object;
        for (String string : list) {
            if (current instanceof JsonObject) current = ((JsonObject) current).get(string);
        }
        String last = current.toString();
        if (last.startsWith("\"") && last.endsWith("\"")) {
            last = last.substring(1, last.length()-1);
        }
        return last.equalsIgnoreCase("null") ? "None" : last;
    }

}
