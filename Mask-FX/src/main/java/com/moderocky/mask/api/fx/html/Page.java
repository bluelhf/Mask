package com.moderocky.mask.api.fx.html;

import com.google.gson.JsonObject;
import com.moderocky.mask.api.fx.Frame;

public class Page implements Frame {

    final String css;
    final String html;
    final String id;
    final JsonObject object;
    final String content;

    public Page(String id, String styleSheetPath, String htmlPath, JsonObject data) {
        this.id = id;
        this.css = getResource(styleSheetPath);
        this.html = getResource(htmlPath);
        object = data == null ? new JsonObject() : data;
        object.addProperty("stylesheet", css);
        HTMLScanner scanner = new HTMLScanner(data, html);
        content = scanner.create();
    }

    public String getContent() {
        return content;
    }

    @Override
    public String getID() {
        return id;
    }
}
