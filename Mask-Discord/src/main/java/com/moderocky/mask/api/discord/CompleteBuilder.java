package com.moderocky.mask.api.discord;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.MessageEmbed;

public class CompleteBuilder extends MessageBuilder {

    public CompleteBuilder(JsonObject object) {
        String content = object.has("content") && object.get("content").isJsonPrimitive() ? object.get("content").getAsString() : null;
        setContent(content);
        MessageEmbed embed = object.has("embed") && object.get("embed").isJsonObject() ? JsonMessageConverter.CONVERTER.toEmbed(object.getAsJsonObject("embed")) : null;
        setEmbed(embed);
    }

    public static MessageBuilder embed(JsonObject object) {
        return new MessageBuilder().setEmbed(JsonMessageConverter.CONVERTER.toEmbed(object));
    }

    public static MessageBuilder message(JsonObject object) {
        String content = object.has("content") && object.get("content").isJsonPrimitive() ? object.get("content").getAsString() : null;
        MessageEmbed embed = object.has("embed") && object.get("embed").isJsonObject() ? JsonMessageConverter.CONVERTER.toEmbed(object.getAsJsonObject("embed")) : null;
        return new MessageBuilder().setEmbed(embed).setContent(content);
    }

    public static MessageBuilder from(String string) {
        return JsonMessageConverter.CONVERTER.toMessageBuilder((JsonObject) JsonParser.parseString(string));
    }

}
