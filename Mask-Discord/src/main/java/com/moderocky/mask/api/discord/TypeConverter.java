package com.moderocky.mask.api.discord;

import club.minnced.discord.webhook.WebhookClient;
import club.minnced.discord.webhook.send.WebhookEmbed;
import club.minnced.discord.webhook.send.WebhookEmbedBuilder;
import club.minnced.discord.webhook.send.WebhookMessage;
import club.minnced.discord.webhook.send.WebhookMessageBuilder;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.requests.RestAction;
import net.dv8tion.jda.api.requests.restaction.AuditableRestAction;

import java.util.concurrent.atomic.AtomicLong;

public class TypeConverter {

    public static final TypeConverter CONVERTER = new TypeConverter();

    public WebhookMessage clone(Message message) {
        WebhookMessageBuilder builder = new WebhookMessageBuilder();
        if (message.getEmbeds().size() > 0) {
            for (MessageEmbed embed : message.getEmbeds()) {
                WebhookEmbedBuilder embedBuilder = new WebhookEmbedBuilder();
                if (embed.getAuthor() != null)
                    embedBuilder.setAuthor(new WebhookEmbed.EmbedAuthor(embed.getAuthor().getName(), embed.getAuthor().getUrl(), embed.getAuthor().getProxyIconUrl()));
                if (embed.getDescription() != null)
                    embedBuilder.setDescription(embed.getDescription());
                if (embed.getImage() != null)
                    embedBuilder.setImageUrl(embed.getImage().getUrl());
                if (embed.getThumbnail() != null)
                    embedBuilder.setThumbnailUrl(embed.getThumbnail().getUrl());
                if (embed.getFooter() != null)
                    embedBuilder.setFooter(new WebhookEmbed.EmbedFooter(embed.getFooter().getText(), embed.getFooter().getIconUrl()));
                if (embed.getTimestamp() != null)
                    embedBuilder.setTimestamp(embed.getTimestamp());
                for (MessageEmbed.Field field : embed.getFields()) {
                    embedBuilder.addField(new WebhookEmbed.EmbedField(field.isInline(), field.getName() != null ? field.getName() : "", field.getValue() != null ? field.getValue() : ""));
                }
                embedBuilder.setColor(embed.getColorRaw());
                builder.addEmbeds(embedBuilder.build());
            }
        }
        if (message.getContentRaw().length() > 0)
            builder.setContent(message.getContentRaw());
        return builder.build();
    }

    public Message clone(WebhookMessage message) {
        MessageBuilder builder = new MessageBuilder();
        if (message.getEmbeds().size() > 0) {
            for (WebhookEmbed embed : message.getEmbeds()) {
                EmbedBuilder embedBuilder = new EmbedBuilder();
                if (embed.getAuthor() != null)
                    embedBuilder.setAuthor(embed.getAuthor().getName(), embed.getAuthor().getUrl(), embed.getAuthor().getIconUrl());
                if (embed.getDescription() != null)
                    embedBuilder.setDescription(embed.getDescription());
                if (embed.getImageUrl() != null)
                    embedBuilder.setImage(embed.getImageUrl());
                if (embed.getThumbnailUrl() != null)
                    embedBuilder.setThumbnail(embed.getThumbnailUrl());
                if (embed.getFooter() != null)
                    embedBuilder.setFooter(embed.getFooter().getText(), embed.getFooter().getIconUrl());
                if (embed.getTimestamp() != null)
                    embedBuilder.setTimestamp(embed.getTimestamp());
                for (WebhookEmbed.EmbedField field : embed.getFields()) {
                    embedBuilder.addField(field.getName(), field.getValue(), field.isInline());
                }
                if (embed.getColor() != null) embedBuilder.setColor(embed.getColor());
                builder.setEmbed(embedBuilder.build());
            }
        }
        if (message.getContent() != null)
            builder.setContent(message.getContent());
        return builder.build();
    }

    public AuditableRestAction<Webhook> createWebhook(TextChannel channel, String name, String reason) {
        return channel.createWebhook(name).reason(reason);
    }

    public RestAction<Message> sendMessage(Webhook webhook, Message message) {
        try (WebhookClient client = WebhookClient.withUrl(webhook.getUrl())) {
            DummyRestAction<Message> restAction = new DummyRestAction<>(message, webhook.getGuild().getJDA());
            AtomicLong id = new AtomicLong();
            client.send(CONVERTER.clone(message)).whenCompleteAsync((readonlyMessage, throwable) -> {
                id.set(readonlyMessage.getId());
                Message m = webhook.getChannel().retrieveMessageById(readonlyMessage.getId()).complete();
                restAction.internalComplete(m);
            });
            return restAction;
        }
    }

}
