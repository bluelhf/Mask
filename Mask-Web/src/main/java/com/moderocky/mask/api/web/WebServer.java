package com.moderocky.mask.api.web;

import com.moderocky.mask.annotation.API;
import com.moderocky.mask.api.web.event.WebActionEvent;
import com.moderocky.mask.internal.utility.FileManager;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;


/**
 * This class is for creating a basic webserver capable of serving static pages.
 * You can further extend its behaviour using listeners (see {@link WebRequestListener} and
 * {@link #registerListener(Class, WebRequestListener)} for further information.)
 * <p>
 * This does take a generic for your web connection extender (if you are using one)
 * however the generic is not used in some returns. This is to avoid a nasty bit of casting
 * in some of the creator classes in the backend. As you're likely to be the only one
 * creating connections, it is perfectly safe to assume that all WebConnections present
 * in this class or returned by it are of your generic type "C" and castable.
 *
 * @param <C> Your connection type
 */
@API
@SuppressWarnings("all")
public abstract class WebServer<C extends WebConnection> extends Thread implements Runnable {

    private final File webRoot;
    private final boolean debug;
    private final @NotNull List<WebConnection> connectionPool = new ArrayList<>();
    private final @NotNull HashMap<Class<? extends WebActionEvent>, List<WebRequestListener>> eventListenerMap = new HashMap<>();
    private final int port;
    private @NotNull
    final String defaultFile = "index.html";
    private @NotNull
    final String fileNotFound = "404.html";
    private @NotNull
    final String methodNotSupported = "not_supported.html";
    private boolean run = true;

    /**
     * This creates a simple webserver capable of serving static pages.
     * If you would like to create a more complex webserver, you ought to copy this class
     * and use an extender of the {@link WebConnection} abstract class.
     *
     * @param port The web port; 80 is the default one
     * @param webContentRoot The folder path where your website files will be
     */
    public WebServer(int port, String webContentRoot) {
        this.port = port;
        this.debug = false;
        this.webRoot = new File(webContentRoot);
    }

    /**
     * See above - with the option to send debug messages to console during errors.
     *
     * @param port The web port; 80 is the default one
     * @param webContentRoot The folder path where your website files will be
     * @param sendDebugMessages Whether to send debug messages
     */
    public WebServer(int port, String webContentRoot, boolean sendDebugMessages) {
        this.port = port;
        this.debug = sendDebugMessages;
        this.webRoot = new File(webContentRoot);
    }

    protected <X extends WebActionEvent> X callEvent(X event) {
        Class<? extends WebActionEvent> clarse = event.getClass();
        if (!eventListenerMap.containsKey(clarse)) return event;
        List<WebRequestListener> listeners = eventListenerMap.get(clarse);
        eventListenerMap.get(clarse).forEach(listener -> listener.onEvent(event));
        return event;
    }

    public void registerListener(Class<? extends WebActionEvent> event, WebRequestListener<?> listener) {
        if (eventListenerMap.containsKey(event)) {
            List<WebRequestListener> listeners = eventListenerMap.get(event);
            listeners.add(listener);
            eventListenerMap.replace(event, listeners);
        } else {
            List<WebRequestListener> listeners = new ArrayList<>();
            listeners.add(listener);
            eventListenerMap.put(event, listeners);
        }
    }

    public boolean isDebug() {
        return debug;
    }

    public File getWebRootFolder() {
        return webRoot;
    }

    @NotNull
    public String getDefaultFileName() {
        return defaultFile;
    }

    @NotNull
    public String get404() {
        return fileNotFound;
    }

    @NotNull
    public String getMethodNotSupportedFileName() {
        return methodNotSupported;
    }

    @Override
    public void run() {
        try {
            webRoot.mkdirs();
            Arrays.asList(defaultFile, fileNotFound, methodNotSupported).forEach(string -> FileManager.putIfAbsent(new File(webRoot, string)));
            ServerSocket serverConnect = new ServerSocket(port);
            if (debug) {
                System.out.println("Webserver opened on port " + port + ".");
            }

            while (run) {
                C webConnection = createConnection(serverConnect.accept());
                if (debug) {
                    System.out.println("Connection opened. (" + new Date() + ")");
                }
                webConnection.start();
//                Thread thread = new Thread(webConnection);
//                thread.start();
            }

        } catch (IOException e) {
            if (debug) {
                System.out.println("Server connection error: " + e.getMessage());
            }
        }
    }

    /**
     * @param socket The socket
     * @return new WebConnection(socket, this) {};
     */
    public abstract C createConnection(Socket socket);

    public int getPort() {
        return port;
    }

    public boolean hasConnections() {
        return !connectionPool.isEmpty();
    }

    protected void addConnection(WebConnection connection) {
        connectionPool.add(connection);
    }

    protected void removeConnection(WebConnection connection) {
        connectionPool.remove(connection);
    }

    @NotNull
    public List<WebConnection> getConnectionPool() {
        return connectionPool;
    }

    public void shutDown() {
        run = false;
    }

}
