package com.moderocky.mask.api.web.event;

import com.moderocky.mask.api.web.Method;
import com.moderocky.mask.api.web.WebConnection;
import com.moderocky.mask.api.web.WebServer;
import org.jetbrains.annotations.NotNull;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.HashMap;

public class PostEvent extends ResponseEvent {

    public PostEvent(@NotNull WebServer<?> server, @NotNull WebConnection connection, @NotNull Socket socket, BufferedOutputStream outputStream, BufferedReader reader, PrintWriter writer, String requested) {
        super(server, connection, socket, Method.POST, outputStream, reader, writer, requested);
    }

    public @NotNull HashMap<String, String> getFormInput() {
        HashMap<String, String> map = new HashMap<>();
        BufferedReader reader = getReader();
        while (true) {
            try {
                if ((reader.readLine()) == null) break;
            } catch (IOException ignore) {
            }
        }
        while (true) {
            try {
                if ((reader.readLine()) == null) break;
            } catch (IOException ignore) {
            }
        }
        StringBuilder payload = new StringBuilder();
        while (true) {
            try {
                if (!reader.ready()) break;
                payload.append((char) reader.read());
            } catch (IOException e) {
                //
            }
        }
        String result = payload.toString().trim();
        String[] lines = result.split(System.lineSeparator());
        for (String line : lines) {
            if (line.trim().length() < 1) continue;
            if (!line.contains("=")) continue;
            map.put(line.split("=")[0], line.split("=")[1]);
        }
        return map;
    }

}
