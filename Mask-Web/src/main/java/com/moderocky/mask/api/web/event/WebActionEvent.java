package com.moderocky.mask.api.web.event;

import com.moderocky.mask.api.web.Method;
import com.moderocky.mask.api.web.WebServer;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

public abstract class WebActionEvent {

    private final @NotNull WebServer<?> server;
    private final @NotNull Socket socket;
    private final @NotNull Method method;
    private boolean ignore = false;

    protected WebActionEvent(@NotNull WebServer<?> server, @NotNull Socket socket, @NotNull Method method) {
        this.server = server;
        this.socket = socket;
        this.method = method;
    }

    public boolean hasInputStream() {
        try {
            socket.getInputStream();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public InputStream getInputStream() {
        try {
            return socket.getInputStream();
        } catch (IOException e) {
            throw new RuntimeException();
        }
    }

    public boolean isCancelled() {
        return ignore;
    }

    public void setCancelled(boolean boo) {
        ignore = boo;
    }

    @NotNull
    public Socket getSocket() {
        return socket;
    }

    @NotNull
    public WebServer<?> getServer() {
        return server;
    }

    @NotNull
    public Method getMethod() {
        return method;
    }
}
