package com.moderocky.mask.api.json;

import com.google.gson.JsonObject;
import com.moderocky.mask.api.web.WebConnection;
import com.moderocky.mask.api.web.WebRequestListener;
import com.moderocky.mask.api.web.WebServer;
import com.moderocky.mask.api.web.event.GetEvent;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.net.Socket;
import java.util.HashMap;
import java.util.List;

public abstract class WebJSonAPI extends WebServer<WebConnection> {

    private final @NotNull HashMap<String, APICollector> apiCollectorHashMap = new HashMap<>();

    public WebJSonAPI(int port, String webContentRoot) {
        super(port, webContentRoot);
        registerListener(GetEvent.class, new RequestListener());
    }

    public void registerCollector(@NotNull APICollector collector) {
        apiCollectorHashMap.put(collector.getID(), collector);
    }

    protected APICollector getCollector(String id) {
        return apiCollectorHashMap.get(id);
    }

    protected abstract String getInvalidUseMessage();

    protected abstract String getContent();

    @Override
    public WebConnection createConnection(Socket socket) {
        return new WebConnection(socket, this) {
        };
    }

    public class RequestListener implements WebRequestListener<GetEvent> {

        @Override
        public void onEvent(GetEvent event) {
            String id = event.getFileRequested();
            if (id == null) return;
            String[] tokens = id.split("/");
            try {
                if (tokens.length < 3) {
                    event.sendResponse(getInvalidUseMessage(), "text/html");
                } else {
                    APICollector collector = getCollector(tokens[1]);
                    if (collector == null) {
                        event.sendResponse(getInvalidUseMessage(), "text/html");
                        return;
                    }
                    @NotNull List<JSonProperty> properties = collector.getProperties(tokens[2]);
                    JsonObject object = new JsonObject();
                    for (JSonProperty property : properties) {
                        Object value = property.getValue();
                        if (value instanceof String)
                            object.addProperty(property.getName(), (String) value);
                        else if (value instanceof Boolean)
                            object.addProperty(property.getName(), (Boolean) value);
                        else if (value instanceof Number)
                            object.addProperty(property.getName(), (Number) value);
                        else if (value instanceof Character)
                            object.addProperty(property.getName(), (Character) value);
                        else
                            object.addProperty(property.getName(), value.toString());
                    }
                    event.sendResponse(object.toString(), "text/plain");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

}
