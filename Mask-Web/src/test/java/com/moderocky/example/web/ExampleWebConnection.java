package com.moderocky.example.web;

import com.moderocky.mask.api.web.WebConnection;
import com.moderocky.mask.api.web.WebServer;

import java.net.Socket;

public class ExampleWebConnection extends WebConnection { // Not much to do here

    public ExampleWebConnection(Socket socket, WebServer<? extends WebConnection> webServer) {
        super(socket, webServer);
    }

}
