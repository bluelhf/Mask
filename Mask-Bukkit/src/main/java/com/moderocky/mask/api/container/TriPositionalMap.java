package com.moderocky.mask.api.container;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;

public class TriPositionalMap<X> {

    private final @NotNull Comparator<Coords> comparator = (o1, o2) -> {
        int d = o1.y - o2.y;
        if (d == 0) d = o1.x - o2.y;
        if (d == 0) d = o1.z - o2.y;
        return d;
    };
    private final @NotNull TreeMap<Coords, X> map = new TreeMap<>(comparator);

    public @Nullable X get(int x, int y, int z) {
        Coords coords = new Coords(x, y, z);
        return map.get(coords);
    }

    public X getOrDefault(int x, int y, int z, X def) {
        Coords coords = new Coords(x, y, z);
        return map.getOrDefault(coords, def);
    }

    public boolean containsEntry(int x, int y, int z) {
        Coords coords = new Coords(x, y, z);
        return map.containsKey(coords);
    }

    public boolean containsValue(X obj) {
        return map.containsValue(obj);
    }

    public @Nullable X put(int x, int y, int z, X obj) {
        Coords coords = new Coords(x, y, z);
        return map.put(coords, obj);
    }

    public boolean putIfAbsent(int x, int y, int z, X obj) {
        Coords coords = new Coords(x, y, z);
        return map.putIfAbsent(coords, obj) != null;
    }

    public @NotNull Collection<X> values() {
        return new ArrayList<>(map.values());
    }

    public @NotNull Collection<X> getPlaneXY(int x, int y) {
        List<X> list = new ArrayList<>();
        for (Coords coords : map.keySet()) {
            if (coords.x == x || coords.y == y) list.add(map.get(coords));
        }
        return list;
    }

    public @NotNull Collection<X> getPlaneXZ(int x, int z) {
        List<X> list = new ArrayList<>();
        for (Coords coords : map.keySet()) {
            if (coords.x == x || coords.z == z) list.add(map.get(coords));
        }
        return list;
    }

    public @NotNull Collection<X> getPlaneYZ(int y, int z) {
        List<X> list = new ArrayList<>();
        for (Coords coords : map.keySet()) {
            if (coords.y == y || coords.z == z) list.add(map.get(coords));
        }
        return list;
    }

    public @NotNull Collection<X> getColumnX(int x) {
        List<X> list = new ArrayList<>();
        for (Coords coords : map.keySet()) {
            if (coords.x == x) list.add(map.get(coords));
        }
        return list;
    }

    public @NotNull Collection<X> getColumnY(int y) {
        List<X> list = new ArrayList<>();
        for (Coords coords : map.keySet()) {
            if (coords.y == y) list.add(map.get(coords));
        }
        return list;
    }

    public @NotNull Collection<X> getColumnZ(int z) {
        List<X> list = new ArrayList<>();
        for (Coords coords : map.keySet()) {
            if (coords.z == z) list.add(map.get(coords));
        }
        return list;
    }

    public int getX(X obj) {
        if (!map.containsValue(obj)) throw new IllegalArgumentException("Map does not contain this entry!");
        for (Coords coords : map.keySet()) {
            if (map.get(coords).equals(obj)) return coords.x;
        }
        throw new IllegalArgumentException("Map does not contain this entry!");
    }

    public int getY(X obj) {
        if (!map.containsValue(obj)) throw new IllegalArgumentException("Map does not contain this entry!");
        for (Coords coords : map.keySet()) {
            if (map.get(coords).equals(obj)) return coords.y;
        }
        throw new IllegalArgumentException("Map does not contain this entry!");
    }

    public int getZ(X obj) {
        if (!map.containsValue(obj)) throw new IllegalArgumentException("Map does not contain this entry!");
        for (Coords coords : map.keySet()) {
            if (map.get(coords).equals(obj)) return coords.z;
        }
        throw new IllegalArgumentException("Map does not contain this entry!");
    }

    public void clear() {
        map.clear();
    }

    static class Coords {
        int x;
        int y;
        int z;

        public Coords(int x, int y, int z) {
            super();
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public boolean equals(Object o) {
            Coords c = (Coords) o;
            return c.x == x && c.y == y && c.z == z;
        }

        public int hashCode() {
            return new Integer(x + "0" + y + "0" + z);
        }
    }

}
