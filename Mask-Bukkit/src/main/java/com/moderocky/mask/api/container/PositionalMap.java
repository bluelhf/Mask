package com.moderocky.mask.api.container;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;

public class PositionalMap<X> {

    private final @NotNull Comparator<Coords> comparator = (o1, o2) -> {
        int d = o1.y - o2.y;
        if (d == 0) d = o1.x - o2.y;
        return d;
    };
    private final @NotNull TreeMap<Coords, X> map = new TreeMap<>(comparator);

    public @Nullable X get(int x, int y) {
        Coords coords = new Coords(x, y);
        return map.get(coords);
    }

    public X getOrDefault(int x, int y, X def) {
        Coords coords = new Coords(x, y);
        return map.getOrDefault(coords, def);
    }

    public boolean containsEntry(int x, int y) {
        Coords coords = new Coords(x, y);
        return map.containsKey(coords);
    }

    public boolean containsValue(X obj) {
        return map.containsValue(obj);
    }

    public @Nullable X put(int x, int y, X obj) {
        Coords coords = new Coords(x, y);
        return map.put(coords, obj);
    }

    public boolean putIfAbsent(int x, int y, X obj) {
        Coords coords = new Coords(x, y);
        return map.putIfAbsent(coords, obj) != null;
    }

    public @NotNull Collection<X> values() {
        return new ArrayList<>(map.values());
    }

    public @NotNull Collection<X> valuesByColumn(int x) {
        List<X> list = new ArrayList<>();
        for (Coords coords : map.keySet()) {
            if (coords.x == x) list.add(map.get(coords));
        }
        return list;
    }

    public @NotNull Collection<X> valuesByRow(int y) {
        List<X> list = new ArrayList<>();
        for (Coords coords : map.keySet()) {
            if (coords.y == y) list.add(map.get(coords));
        }
        return list;
    }

    public int getX(X x) {
        if (!map.containsValue(x)) throw new IllegalArgumentException("Map does not contain this entry!");
        for (Coords coords : map.keySet()) {
            if (map.get(coords).equals(x)) return coords.x;
        }
        throw new IllegalArgumentException("Map does not contain this entry!");
    }

    public int getY(X x) {
        if (!map.containsValue(x)) throw new IllegalArgumentException("Map does not contain this entry!");
        for (Coords coords : map.keySet()) {
            if (map.get(coords).equals(x)) return coords.y;
        }
        throw new IllegalArgumentException("Map does not contain this entry!");
    }

    public void clear() {
        map.clear();
    }

    static class Coords {
        int x;
        int y;

        public Coords(int x, int y) {
            super();
            this.x = x;
            this.y = y;
        }

        public boolean equals(Object o) {
            Coords c = (Coords) o;
            return c.x == x && c.y == y;
        }

        public int hashCode() {
            return new Integer(x + "0" + y);
        }
    }

}
