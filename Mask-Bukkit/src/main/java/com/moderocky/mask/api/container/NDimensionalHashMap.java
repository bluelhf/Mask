package com.moderocky.mask.api.container;

import org.jetbrains.annotations.NotNull;

import java.lang.reflect.ParameterizedType;
import java.util.*;

/**
 * A map designed to store values keyed to an array of properties.
 * When queried, the query is done based on the property array using seperate criteria.
 * <p>
 * You are able to query for an exact match using {@link #get(Object)} or
 * instead query with a single property or collection of partially-matching properties
 * in order to get relevant entries. See the following:
 * <p>
 * {@link #getAnyMatching(Object[])}
 * {@link #getEntries(Object[])}
 * {@link #getEntries(Object, int)}
 * <p>
 * Remember that the N-Dimensional HashMap requires an explicit number of dimensions
 * in order to configure the correct property array.
 * All property key entries must be of the same length.
 * Use null-values as individual array members if properties are unknown.
 *
 * @param <Q> Key array type
 * @param <V> Value type
 */
public class NDimensionalHashMap<Q, V> extends HashMap<Q[], V> {

    private final int dimensions;

    public NDimensionalHashMap(int dimensions) {
        this.dimensions = dimensions;
    }

    /**
     * Retrieve a single value based on an exact match.
     *
     * @param key An array-type of {@link Q}, with the exact length of {@link #getDimensions()}.
     * @return The value, otherwise null
     */
    @Override
    public V get(Object key) {
        if (key == null || !key.getClass().isArray()) throw new IllegalArgumentException("Key is not plural.");
        Object[] objects = (Object[]) key;
        if (objects.length != dimensions)
            throw new IllegalArgumentException("Key does not have the correct number of dimensions.");
        Class<Q[]> type = (Class<Q[]>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        return super.get(key);
    }

    /**
     * @param key An array-type of {@link Q}, with the exact length of {@link #getDimensions()}.
     */
    @Override
    public V put(Q[] key, V value) {
        if (key.length != dimensions)
            throw new IllegalArgumentException("Key does not have the correct number of dimensions.");
        return super.put(key, value);
    }

    @Override
    public void putAll(Map<? extends Q[], ? extends V> m) {
        super.putAll(m);
    }

    @Override
    public V getOrDefault(Object key, V defaultValue) {
        if (key == null || !key.getClass().isArray()) throw new IllegalArgumentException("Key is not plural.");
        Object[] objects = (Object[]) key;
        if (objects.length != dimensions)
            throw new IllegalArgumentException("Key does not have the correct number of dimensions.");
        return super.getOrDefault(key, defaultValue);
    }

    @Override
    public V putIfAbsent(Q[] key, V value) {
        if (key.length != dimensions)
            throw new IllegalArgumentException("Key does not have the correct number of dimensions.");
        return super.putIfAbsent(key, value);
    }

    @Override
    public boolean replace(Q[] key, V oldValue, V newValue) {
        if (key.length != dimensions)
            throw new IllegalArgumentException("Key does not have the correct number of dimensions.");
        return super.replace(key, oldValue, newValue);
    }

    @Override
    public V replace(Q[] key, V value) {
        if (key.length != dimensions)
            throw new IllegalArgumentException("Key does not have the correct number of dimensions.");
        return super.replace(key, value);
    }

    public int getDimensions() {
        return dimensions;
    }

    public @NotNull Collection<V> getEntries(Q property, int position) {
        List<V> list = new ArrayList<>();
        keySet().forEach(qs -> {
            if (qs[position].equals(property)) list.add(get(qs));
        });
        return list;
    }

    public @NotNull Collection<V> getEntries(Q[] properties) {
        List<Q> qList = Arrays.asList(properties);
        List<V> list = new ArrayList<>();
        keySet().forEach(qs -> {
            int i = 0;
            for (Q q : qs) {
                if (qList.contains(q)) i++;
            }
            if (i == qList.size()) list.add(get(qs));
        });
        return list;
    }

    public @NotNull Collection<V> getAnyMatching(Q[] properties) {
        List<Q> qList = Arrays.asList(properties);
        List<V> list = new ArrayList<>();
        keySet().forEach(qs -> {
            for (Q q : qs) {
                if (qList.contains(q)) {
                    list.add(get(qs));
                    break;
                }
            }
        });
        return list;
    }

}
