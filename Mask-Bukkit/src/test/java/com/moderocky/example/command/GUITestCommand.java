package com.moderocky.example.command;

import com.moderocky.example.MainBukkitPluginClass;
import com.moderocky.mask.command.Commander;
import com.moderocky.mask.gui.ItemFactory;
import com.moderocky.mask.gui.MenuGUI;
import com.moderocky.mask.template.WrappedCommand;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class GUITestCommand extends Commander<Player> implements WrappedCommand {

    @Override
    public @NotNull Commander.CommandImpl create() {
        return command("gui_test");
    }

    @Override
    public @NotNull CommandSingleAction<Player> getDefault() { // This provides a help message with clickable options for ALL possible arguments
        MenuGUI gui = new MenuGUI(MainBukkitPluginClass.getInstance(), 18, "Test InventoryGUI")
                .createTile(
                        new ItemFactory(Material.BLACK_STAINED_GLASS_PANE)
                                .addConsumer(meta -> meta.setDisplayName("§0"))
                                .create(),
                        1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 16
                )
                .createButton(4, new ItemFactory(Material.TOTEM_OF_UNDYING)
                                .addConsumer(meta -> {
                                    meta.setDisplayName("§fClick to Die!");
                                    meta.setLore(Arrays.asList("Lore", "lore 2"));
                                }).create(),
                        (player, event) -> player.sendMessage("it works :o")
                )
                .createButton(17, new ItemFactory(Material.EMERALD_BLOCK)
                                .setAmount(8)
                                .addConsumer(meta -> {
                                    meta.setDisplayName("§fClick to Die!");
                                    meta.setLore(Arrays.asList("Lore", "lore 2"));
                                }).create(),
                        (player, event) -> player.sendMessage("it works :o")
                )
                .createButton(16, new ItemFactory(Material.TOTEM_OF_UNDYING)
                                .addConsumer(meta -> {
                                    meta.setDisplayName("§fClick to Die!");
                                    meta.setLore(Arrays.asList("Lore", "lore 2"));
                                }).create(),
                        (player, event) -> player.sendMessage("it works :o")
                );
        return gui::open;
    }

    @Override
    public @Nullable String getPermission() {
        return "test.command.permission";
    }

    @Override
    public @Nullable String getPermissionMessage() {
        return null;
    }

    @Override
    public @NotNull List<String> getAliases() {
        return Arrays.asList("gui", "menutest");
    }

    @Override
    public @NotNull String getUsage() {
        return "/" + getCommand();
    }

    @Override
    public @NotNull String getDescription() {
        return "Description here";
    }

    @Override
    public @Nullable List<String> getCompletions(int i) { // Not needed - we're using the parent.
        return null;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        if (sender instanceof Player) {
            return execute((Player) sender, args);
        }
        return false;
    }

    @Override
    public @Nullable List<String> onTabComplete(@NotNull CommandSender sender, @NotNull Command command, @NotNull String alias, @NotNull String[] args) {
        List<String> strings = getPossibleArguments(String.join(" ", args));
        if (strings == null || strings.isEmpty()) return null;
        final List<String> completions = new ArrayList<>();
        StringUtil.copyPartialMatches(args[args.length - 1], strings, completions);
        Collections.sort(completions);
        return completions;
    }

}
