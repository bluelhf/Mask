package com.moderocky.example.reflection;

import dev.moderocky.mirror.Mirror;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.regex.Pattern;

public class BlindReflectionExample {

    public void myReflectionTest() {
        { // Using Mirror
            Mirror<Object> nbt = new Mirror<>(Mirror.mirror("net.minecraft.server.v1_15_R1.NBTTagCompound").instantiate());
            nbt.invoke("setString", "MyString", "test value");
            Pattern regex = nbt.<Pattern>field("c").get();
            nbt.<Pattern>field("c").set(regex);
        } // Concerned about exceptions? Use Mirror's tryCatch() system to silently handle runtime exceptions! :)

        { // Using normal reflection
            try {
                Class<?> cls = Class.forName("net.minecraft.server.v1_15_R1.NBTTagCompound");
                Object nbt = cls.newInstance();
                Method setter = nbt.getClass().getDeclaredMethod("setString", String.class, String.class);
                if (!setter.isAccessible()) setter.setAccessible(true);
                setter.invoke(nbt, "MyString", "test value");
                Field field = nbt.getClass().getDeclaredField("c");
                if (!field.isAccessible()) field.setAccessible(true);
                Pattern regex = (Pattern) field.get(nbt);
                Field mod = field.getClass().getDeclaredField("modifiers");
                mod.setAccessible(true);
                mod.setInt(field, field.getModifiers() & ~Modifier.FINAL);
                field.set(nbt, regex);
            } catch (InstantiationException | InvocationTargetException | NoSuchMethodException | IllegalAccessException | NoSuchFieldException | ClassNotFoundException e) {
                e.printStackTrace(); // Huh, that's a lot of exceptions...
            }
        }
    }

    public void anotherTest() {
        Player player = Bukkit.getOnlinePlayers().iterator().next();
        { // Using Mirror
            Mirror<?> nmsPlayer = new Mirror<>(new Mirror<>(player).invoke("getHandle"));
            nmsPlayer.field("lastHealthScored").set(10.0F);
            int exp = nmsPlayer.<Integer>field("newLevel").get();
            nmsPlayer.ifHasField("cq", field -> field.set(true));
        }

        {
            try {
                Class<?> cls = Class.forName("org.bukkit.craftbukkit.v1_15_R1.entity.CraftPlayer");
                Method handle = cls.getDeclaredMethod("getHandle");
                handle.setAccessible(true);
                Object nmsPlayer = handle.invoke(player);
                Field lhs = nmsPlayer.getClass().getDeclaredField("lastHealthScored");
                lhs.setAccessible(true);
                lhs.setFloat(nmsPlayer, 10.0F);
                Field xpf = nmsPlayer.getClass().getDeclaredField("newLevel");
                xpf.setAccessible(true);
                int exp = xpf.getInt(nmsPlayer);
                try {
                    Field cq = nmsPlayer.getClass().getDeclaredField("cq");
                    cq.setAccessible(true);
                    cq.setBoolean(nmsPlayer, true);
                } catch (NoSuchFieldException e) {
                    // ignore
                }
            } catch (InvocationTargetException | NoSuchMethodException | IllegalAccessException | NoSuchFieldException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

}
