package com.moderocky.example;

import com.moderocky.example.command.*;
import com.moderocky.example.listener.ExampleListener;
import com.moderocky.mask.Mask;
import com.moderocky.mask.MaskAPI;
import com.moderocky.mask.template.BukkitPlugin;

public class MainBukkitPluginClass extends BukkitPlugin {
    // The config will be loaded from file and save if non-existent.
    // We can use config.save() and config.load() to save/reload it where necessary.


    @Override
    public void startup() { // Called at enable.
        register(new ExampleListener());
        register( //complete
                new ExampleCommand()
        );
        register( //wrapped
                new VisualGUITestCommand(),
                new GUITestCommand(),
                new ExampleCommander(),
                new ExampleWrappedCommand()
        );

        MaskAPI api = Mask.getMask().getAPI();


    }

    @Override
    public void disable() { // Called at disable.

    }

}
