package com.moderocky.mask.api.proc;

import java.util.concurrent.*;
import java.util.function.Consumer;

public class Processor {

    public static final ScheduledExecutorService SCHEDULER = Executors.newScheduledThreadPool(2);
    public static final ExecutorService THREAD_POOL = Executors.newFixedThreadPool(4);
    public static final BlockingQueue<Task> queue = new LinkedBlockingQueue<>();
    public static final Thread THREAD = Thread.currentThread();

    public static void execute(Task task) {
        execute(task, null);
    }

    public static void execute(Task task, Consumer<Throwable> failure) {
        try {
            if (task.isAsync()) {
                THREAD_POOL.execute(task);
            } else if (task.isSync()) {
                queue.add(task);
                while (queue.size() > 0) queue.take().run();
            }
        } catch (Throwable throwable) {
            if (failure != null) failure.accept(throwable);
        }
    }

}
