package com.moderocky.mask.api;

public class StringProcessor extends StringReader {
    StringProcessor(MagicStringList list) {
        super(list);
    }

    public StringProcessor(String string) {
        this(string.toCharArray());
    }

    public StringProcessor(char[] chars) {
        super(chars);
    }

    public boolean readMatches(CharSequence sequence) {
        final char[] chars = sequence.toString().toCharArray();
        for (char c : chars) {
            if (!canRead()) return false;
            if (c != rotate()) return false;
        }
        return true;
    }

    public boolean followsWith(CharSequence sequence) {
        StringReader reader = this.clone();
        final char[] chars = sequence.toString().toCharArray();
        for (char c : chars) {
            if (!canRead()) return false;
            if (c != reader.rotate()) return false;
        }
        return true;
    }

    public StringProcessor readOut(CharSequence sequence) {
        final char[] chars = sequence.toString().toCharArray();
        for (char c : chars) {
            if (!canRead()) return this;
            if (c != rotate()) return this;
        }
        return this;
    }

    public String[] getCatchment(ScanningPattern pattern) {
        return pattern.clone().runWith(this.clone());
    }

    public boolean matches(ScanningPattern pattern) {
        return matches(pattern, false);
    }

    public boolean matches(ScanningPattern pattern, boolean lenient) {
        StringProcessor processor = this.clone();
        boolean skip = lenient;
        for (ScanningPattern.Part part : pattern.order) {
            if (part instanceof ScanningPattern.Static) {
                CharSequence string = ((ScanningPattern.Static) part).string;
                if (skip) {
                    readUntil(string);
                    skip = false;
                }
                if (!processor.followsWith(string)) return false;
                readOut(string);
            } else if (part instanceof ScanningPattern.Catcher) {
                skip = true;
            }
        }
        return true;
    }

    public String readOutUntil(CharSequence sequence) {
        StringBuilder builder = new StringBuilder();
        if (sequence.length() < 1) {
            return builder.toString();
        }
        char c = sequence.charAt(0);
        while (canRead() && (current() != c) && !followsWith(sequence)) builder.append(rotate());
        return builder.toString();
    }

    public StringProcessor readUntil(CharSequence sequence) {
        if (sequence.length() < 1) {
            return this;
        }
        char c = sequence.charAt(0);
        while (canRead() && (current() != c) && !followsWith(sequence)) rotate();
        return this;
    }

    public StringProcessor readUntilAfter(CharSequence sequence) {
        while (canRead() && !followsWith(sequence)) rotate();
        readOut(sequence);
        return this;
    }

    public StringProcessor skipWhitespace() {
        while (canRead() && (current() == ' ' || current() == '\t' || current() == '\n' || current() == '\r')) rotate();
        return this;
    }

    public BracketReader ofAll() {
        return new BracketReader(this);
    }

    public BracketReader ofRemaining() {
        return new BracketReader(this.remaining());
    }

    @Override
    @SuppressWarnings("all")
    public StringProcessor clone() {
        StringProcessor reader = new StringProcessor(chars);
        reader.position = position;
        return reader;
    }


}
