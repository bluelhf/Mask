package com.moderocky.mask.api.data;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import dev.moderocky.mirror.Mirror;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;

public class FixedNDimensionalTable<P> implements ITable<P> {

    protected final int[] dimensions;
    protected final Class<P> cls;
    protected final @NotNull Object table;
    protected String name;

    public FixedNDimensionalTable(Class<P> cls, int... dimensions) {
        this.dimensions = dimensions;
        this.cls = cls;
        table = Array.newInstance(cls, dimensions);
    }

    @SuppressWarnings("unchecked")
    private FixedNDimensionalTable(@NotNull Object structure) {
        assert structure.getClass().isArray();
        Object object = structure;
        int i = 0;
        int[] ints = new int[255];
        while (object.getClass().isArray()) {
            ints[i] = Array.getLength(object);
            object = object.getClass().getComponentType();
            i++;
        }
        this.dimensions = Arrays.copyOf(ints, i + 1);
        this.table = structure;
        cls = (Class<P>) object;
    }

    private FixedNDimensionalTable(@NotNull Object table, int[] dimensions, Class<P> cls, String name) {
        this.name = name;
        this.dimensions = dimensions;
        this.cls = cls;
        this.table = table;
    }

    protected FixedNDimensionalTable(FixedNDimensionalTable<P> table) {
        this.dimensions = table.dimensions;
        this.cls = table.cls;
        this.table = table.table;
        this.name = table.name;
    }

    public static <T> FixedNDimensionalTable<T> from(File file) {
        return from(file, null);
    }

    public static <T> FixedNDimensionalTable<T> from(String content) {
        return from(content, null);
    }

    public static <T> FixedNDimensionalTable<T> from(File file, @Nullable FixedNDimensionalTable<T> def) {
        String content = ITable.readContent(file);
        return from(content, def);
    }

    public static void to(File file, FixedNDimensionalTable<?> table) {
        ITable.writeContent(file, table.serialise());
    }

    public static <T> FixedNDimensionalTable<T> from(String content, @Nullable FixedNDimensionalTable<T> def) {
        final int[] dimensions;
        if (content == null) return def;
        try {
            JsonObject object = JsonParser.parseString(content).getAsJsonObject();
            Class<T> cls = Mirror.getClass(object.get("class").getAsString());
            JsonArray size = object.getAsJsonArray("size");
            dimensions = new int[size.size()];
            for (int i = 0; i < size.size(); i++) {
                dimensions[i] = size.get(i).getAsInt();
            }
            final Object template = Array.newInstance(cls, dimensions);
            final Object table = GSON.fromJson(object.get("table"), template.getClass());
            final T defaultValue = GSON.fromJson(object.get("default"), cls);
            return new FixedNDimensionalTable<T>(table, dimensions, cls, object.get("name").isJsonNull() ? null : object.get("name").getAsString()) {
                @Nullable
                @Override
                public T defaultValue() {
                    return defaultValue;
                }
            };
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            return def;
        }
    }

    public JsonObject toJson() {
        final JsonObject object = new JsonObject();
        JsonArray size = new JsonArray();
        for (int dimension : dimensions) {
            size.add(dimension);
        }
        object.addProperty("name", name);
        object.addProperty("class", cls.getName());
        object.add("size", size);
        object.add("default", GSON.toJsonTree(defaultValue(), cls));
        object.add("table", GSON.toJsonTree(table, table.getClass()));
        return object;
    }

    public String serialise() {
        return toJson().toString();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @SuppressWarnings("unchecked")
    public P get(int... position) throws IndexOutOfBoundsException {
        Object object = table;
        for (int i : position) {
            if (object.getClass().isArray())
                object = Array.get(object, i);
        }
        return (P) object;
    }

    @SuppressWarnings("unchecked")
    public <Q> Q getCast(int... position) throws IndexOutOfBoundsException {
        return (Q) get(position);
    }

    public void set(final P value, int... position) throws IndexOutOfBoundsException {
        Object object = table;
        for (int i : position) {
            if (object.getClass().isArray() && object.getClass().getComponentType().isArray())
                object = Array.get(object, i);
        }
        if (object.getClass().isArray())
            Array.set(object, position[position.length - 1], value);
    }

    public Class<P> getComponentType() {
        return cls;
    }

    public Object[] getTable() {
        return copy((Object[]) table);
    }

    @SuppressWarnings("unchecked")
    private <Q> Q[] copy(Q[] original) {
        final Q[] copy = (Q[]) Array.newInstance(original.getClass().getComponentType(), original.length);
        System.arraycopy(original, 0, copy, 0, original.length);
        return copy;
    }

    public @Nullable P defaultValue() {
        return null;
    }

    @Override
    public P[] asLinearRepresentation() {
        final P[] array = ITable.newMultiplicativeArray(cls, dimensions);
        final int[] counter = new int[1];
        Object[] objects = (Object[]) table;
        addTo(objects, array, counter);
        return array;
    }

    private void addTo(final Object[] array, final P[] target, final int[] counter) {
        if (array.getClass().getComponentType().isArray()) {
            for (Object o : array) {
                addTo((Object[]) o, target, counter);
            }
        } else {
            for (Object o : array) {
                if (o == null) {
                    target[counter[0]] = null;
                    counter[0]++;
                } else if (o.getClass().isArray()) {
                    addTo((Object[]) o, target, counter);
                } else {
                    target[counter[0]] = cls.cast(o);
                    counter[0]++;
                }
            }
        }
    }

}
