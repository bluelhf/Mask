package com.moderocky.mask.api;

import java.util.Collection;

public class MagicNumberList<T extends Number> extends MagicList<T> {

    public MagicNumberList(Collection<T> collection) {
        super(collection);
    }

    public MagicNumberList() {
        super();
    }

    @SafeVarargs
    public MagicNumberList(T... ts) {
        super(ts);
    }

    public double[] toDoubleArray() {
        Double[] bigs = super.toArray(new Double[0]);
        double[] smalls = new double[bigs.length];
        for (int i = 0; i < bigs.length; i++) {
            smalls[i] = bigs[i];
        }
        return smalls;
    }

    public long[] toLongArray() {
        Long[] bigs = super.toArray(new Long[0]);
        long[] smalls = new long[bigs.length];
        for (int i = 0; i < bigs.length; i++) {
            smalls[i] = bigs[i];
        }
        return smalls;
    }

    public int[] toIntArray() {
        Integer[] bigs = super.toArray(new Integer[0]);
        int[] smalls = new int[bigs.length];
        for (int i = 0; i < bigs.length; i++) {
            smalls[i] = bigs[i];
        }
        return smalls;
    }

    public short[] toShortArray() {
        Short[] bigs = super.toArray(new Short[0]);
        short[] smalls = new short[bigs.length];
        for (int i = 0; i < bigs.length; i++) {
            smalls[i] = bigs[i];
        }
        return smalls;
    }

}
