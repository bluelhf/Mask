package com.moderocky.mask.api;

import java.util.Collection;

public class FixedMagicList<T> extends MagicList<T> {

    protected final int size;

    public FixedMagicList(int size) {
        this.size = size;
    }

    public FixedMagicList(Collection<? extends T> collection) {
        super(collection);
        this.size = collection.size();
    }

    public FixedMagicList(int i, Collection<? extends T> collection) {
        super(collection);
        if (this.size() < collection.size()) throw new IllegalArgumentException("Lock size is less than collection size!");
        this.size = i;
    }

    @SafeVarargs
    public FixedMagicList(T... ts) {
        super(ts);
        this.size = ts.length;
    }

    @SafeVarargs
    public FixedMagicList(int i, T... ts) {
        super(ts);
        if (this.size() < ts.length) throw new IllegalArgumentException("Lock size is less than array size!");
        this.size = i;
    }

    @Override
    public boolean add(T t) {
        if (size() < size)
            return super.add(t);
        return false;
    }

    @Override
    public void add(int index, T element) {
        if (index >= size) throw new IllegalArgumentException("Index is outside the lock size!");
        super.add(index, element);
        while (size() > size) remove(size);
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        final int count = modCount;
        for (T t : c) {
            add(t);
        }
        return count != modCount;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        if (index >= size) throw new IllegalArgumentException("Index is outside the lock size!");
        final int count = modCount;
        for (T t : c) {
            super.add(index, t);
        }
        while (size() > size) remove(size);
        return count != modCount;
    }
}
