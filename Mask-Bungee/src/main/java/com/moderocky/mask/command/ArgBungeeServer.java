package com.moderocky.mask.command;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Moderocky
 * @version 1.0.0
 */
public class ArgBungeeServer implements Argument<ServerInfo> {

    private String label = "server";
    private boolean required = true;

    @Override
    public @NotNull ServerInfo serialise(String string) {
        ServerInfo info = ProxyServer.getInstance().getServerInfo(string);
        if (info == null) throw new IllegalArgumentException();
        return info;
    }

    @Override
    public boolean matches(String string) {
        return (ProxyServer.getInstance().getServerInfo(string) != null);
    }

    @Override
    public @NotNull String getName() {
        return label;
    }

    @Override
    public @Nullable List<String> getCompletions() {
        return new ArrayList<>(ProxyServer.getInstance().getServers().keySet());
    }

    @Override
    public boolean isPlural() {
        return false;
    }

    @Override
    public boolean isRequired() {
        return required;
    }

    @Override
    public ArgBungeeServer setRequired(boolean boo) {
        required = boo;
        return this;
    }

    @Override
    public ArgBungeeServer setLabel(@NotNull String label) {
        this.label = label;
        return this;
    }

}