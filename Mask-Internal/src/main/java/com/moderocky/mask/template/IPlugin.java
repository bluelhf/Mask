package com.moderocky.mask.template;

import com.moderocky.mask.annotation.API;
import dev.moderocky.mirror.Mirror;

import java.io.InputStream;
import java.lang.reflect.Method;

public interface IPlugin {

    /**
     * Called on enable.
     */
    @API
    void startup();

    /**
     * Called on disable.
     */
    @API
    void disable();

    @API
    default String getConfigPath() {
        return "plugins/" + getName() + "/config.yml";
    }

    default String getName() {
        try {
            if (isBukkit()) {
                Method method = Mirror.mirror("org.bukkit.plugin.java.JavaPlugin").method("getName").getLiteral();
                return method.invoke(this).toString();
            } else {
                Method method = Mirror.mirror("net.md_5.bungee.api.plugin.Plugin").method("getDescription").getLiteral();
                return new Mirror<>(method.invoke(this)).invoke("getName");
            }
        } catch (Throwable throwable) {
            return this.getClass().getCanonicalName();
        }
    }

    default boolean isBukkit() {
        return getPluginType() == PluginType.BUKKIT;
    }

    default boolean isBungee() {
        return getPluginType() == PluginType.BUNGEE;
    }

    default PluginType getPluginType() {
        return Mirror.classExists("org.bukkit.plugin.java.JavaPlugin") ? PluginType.BUKKIT : PluginType.BUNGEE;
    }

    InputStream getResource(String string);

    enum PluginType {
        BUKKIT,
        BUNGEE
    }

}
