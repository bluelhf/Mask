package com.moderocky.mask.config;

import java.io.File;

public abstract class Configuration<T> implements MemorySection<T>, ConjunctionPart<T> {

    private final File file;

    public Configuration(File file) {
        this.file = file;
    }

    @Override
    public File getFile() {
        return file;
    }
}
