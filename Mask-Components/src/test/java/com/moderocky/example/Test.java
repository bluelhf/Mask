package com.moderocky.example;

import com.moderocky.mask.api.chat.ComponentReader;

import java.time.Instant;

public class Test {

    public static void main(String[] args) {
        final String string = "<#ff0000>blob<#ff0000><show_text='hello'><command='/blob'>blob<#ff0000><show_text='hello'><command='/blob'>blob<#ff0000><show_text='hello'><command='/blob'>blobv<#ff0000><show_text='hello'><command='/blob'>blob<#ff0000><show_text='hello'><command='/blob'>blob<#ff0000><show_text='hello'><command='/blob'>blob<#ff0000><show_text='hello'><command='/blob'>blob<#ff0000><show_text='hello'><command='/blob'>blob<#ff0000><show_text='hello'><command='/blob'>blob<#ff0000><show_text='hello'><command='/blob'>blob";
        for (int i = 0; i < 1; i++) {
            new ComponentReader(string).read();
        }
        Instant a = Instant.now();
        for (int i = 0; i < 100; i++) {
            new ComponentReader(string).read();
        }
        Instant b = Instant.now();
        System.out.println("Time taken: " + (b.toEpochMilli() - a.toEpochMilli()) + "ms");


    }

}
