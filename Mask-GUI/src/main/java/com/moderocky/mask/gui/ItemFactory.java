package com.moderocky.mask.gui;

import dev.moderocky.mirror.MethodMirror;
import dev.moderocky.mirror.Mirror;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * An item-creating factory to allow for easy itemstack creation/manipulation within a single
 * object.
 * This makes use of chain methods as well as consumers to simplify the process.
 */
public class ItemFactory {

    private final @NotNull List<Consumer<ItemMeta>> consumers = new ArrayList<>();
    private final @NotNull List<Consumer<ItemStack>> itemEdits = new ArrayList<>();
    private @NotNull Material material;
    private @NotNull ItemMeta meta;
    private int amount;

    public static final String VERSION;
    public static final Class<?> NBT_CLASS;
    public static final Class<?> ITEM_CLASS;
    public static final Class<?> CRAFT_ITEM_CLASS;
    public static final MethodMirror<?> NBT_PARSER;
    public static final MethodMirror<?> ITEM_CREATOR;
    public static final MethodMirror<ItemStack> ITEM_CONVERTER;

    static {
        VERSION = Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];
        NBT_CLASS = Mirror.getClass("net.minecraft.server." + VERSION + ".NBTTagCompound");
        ITEM_CLASS = Mirror.getClass("net.minecraft.server." + VERSION + ".ItemStack");
        CRAFT_ITEM_CLASS = Mirror.getClass("org.bukkit.craftbukkit." + VERSION + ".inventory.CraftItemStack");
        final Mirror<?> parser = Mirror.mirror("net.minecraft.server." + VERSION + ".MojangsonParser");
        NBT_PARSER = parser.method("parse", String.class);
        final Mirror<?> mirror = new Mirror<>(ITEM_CLASS);
        ITEM_CREATOR = mirror.method("a", NBT_CLASS);
        final Mirror<?> convert = new Mirror<>(CRAFT_ITEM_CLASS);
        ITEM_CONVERTER = convert.<ItemStack>method("asBukkitCopy", ITEM_CLASS);
    }

    /**
     * Creates a default factory for an Air item.
     */
    public ItemFactory() {
        material = Material.AIR;
        amount = 1;
        meta = Bukkit.getItemFactory().getItemMeta(material);
    }

    /**
     * Creates a factory based on an NBT-serialised item.
     * This might not work in future versions!
     */
    public ItemFactory(@NotNull String nbt) {
        this(parse(nbt));
    }

    /**
     * Creates a factory based on an NBT-serialised item.
     * This might not work in future versions!
     *
     * You may specify the name of the ItemStack#methodName(NBTTagCompound) method.
     */
    public ItemFactory(@NotNull String nbt, @NotNull String creatorMethodName) {
        this(parse(nbt, creatorMethodName));
    }

    /**
     * Creates an item factory for the given material.
     *
     * @param material Material
     */
    public ItemFactory(@NotNull Material material) {
        this.material = material;
        this.amount = 1;
        this.meta = Bukkit.getItemFactory().getItemMeta(material);
    }

    /**
     * Creates an item factory for the given material.
     *
     * @param material Material
     * @param amount Amount
     */
    public ItemFactory(@NotNull Material material, int amount) {
        this.material = material;
        this.amount = amount;
        this.meta = Bukkit.getItemFactory().getItemMeta(material);
    }

    /**
     * Creates an item factory for the given material.
     *
     * @param material Material
     * @param consumer A consumer to be performed on the item when {@link #create()} is used.
     */
    public ItemFactory(@NotNull Material material, @NotNull Consumer<ItemMeta> consumer) {
        this.material = material;
        this.amount = 1;
        this.meta = Bukkit.getItemFactory().getItemMeta(material);
        this.consumers.add(consumer);
    }

    /**
     * Creates an item factory for the given material.
     *
     * @param material Material
     * @param amount Amount
     * @param consumer A consumer to be performed on the item's meta when {@link #create()} is used.
     */
    public ItemFactory(@NotNull Material material, int amount, @NotNull Consumer<ItemMeta> consumer) {
        this.material = material;
        this.amount = amount;
        this.meta = Bukkit.getItemFactory().getItemMeta(material);
        this.consumers.add(consumer);
    }

    /**
     * Creates an item factory based on the given item.
     * Remember: any edits will not be applied to this item.
     * <p>
     * You can use {@link #apply(ItemStack)} to apply the edits to an item.
     *
     * @param itemStack The template item stack to use.
     */
    public ItemFactory(@NotNull ItemStack itemStack) {
        this.material = itemStack.getType();
        this.meta = itemStack.getItemMeta();
        this.amount = itemStack.getAmount();
    }

    /**
     * Set the final amount. This will be bounded between 0 and 127 inclusively.
     *
     * @param i Amount
     * @return Chain
     */
    public ItemFactory setAmount(int i) {
        amount = i;
        return this;
    }

    /**
     * Allows a third-party ItemFactory system to be used within construction.
     * You might wish to write your own customised features for this, without needing to extend it
     * or to break the method chaining.
     * <p>
     * This will allow you to do so.
     *
     * @param implementation A blank factory
     * @param <Q> Your factory type
     * @return Your factory, pointing to this item-factory for application
     */
    public <Q extends FactoryImplementation> @NotNull Q accept(@NotNull Q implementation) {
        return implementation.setTarget(this);
    }

    /**
     * Adds a consumer that can be run on the meta upon creation or application.
     * An unlimited number of consumers can be added here.
     *
     * @param consumer A consumer to be run on the item meta
     * @return Chain
     */
    public <M extends ItemMeta> ItemFactory addConsumer(Consumer<M> consumer) {
        consumers.add((Consumer<ItemMeta>) consumer);
        return this;
    }

    /**
     * Adds a consumer that can be run on the persistent data upon creation or application.
     * An unlimited number of consumers can be added here.
     *
     * @param consumer A consumer to be run on the item's persistent data
     * @return Chain
     */
    public ItemFactory addDataConsumer(Consumer<PersistentDataContainer> consumer) {
        consumers.add(meta -> consumer.accept(meta.getPersistentDataContainer()));
        return this;
    }

    /**
     * Adds a consumer that can be run on the item-stack itself upon creation or application.
     * An unlimited number of consumers can be added here.
     * <p>
     * These will be run post-meta application.
     *
     * @param consumer A consumer to be run on the item-stack
     * @return Chain
     */
    public ItemFactory addItemConsumer(Consumer<ItemStack> consumer) {
        itemEdits.add(consumer);
        return this;
    }

    /**
     * Applies this factory to an existing item.
     * If the material differs, the meta will be converted to match this new item.
     * <p>
     * The material of an existing item cannot be changed.
     *
     * @param itemStack An item
     * @return Chain
     */
    public ItemFactory apply(ItemStack itemStack) {
        ItemMeta meta = this.meta.clone();
        try {
            for (Consumer<ItemMeta> consumer : consumers) {
                consumer.accept(meta);
            }
        } catch (Throwable ignore) {
        }
        itemStack.setAmount(Math.max(0, Math.min(127, amount)));
        itemStack.setItemMeta(meta);
        for (Consumer<ItemStack> edit : itemEdits) {
            try {
                edit.accept(itemStack);
            } catch (Throwable ignore) {
            }
        }
        return this;
    }

    /**
     * @return The given material
     */
    public @NotNull Material getMaterial() {
        return material;
    }

    /**
     * Re-sets the material.
     * Will attempt to convert the existing item meta to match the new material.
     * If the meta cannot be converted, it will be ignored.
     *
     * @param material A material
     * @return Chain
     */
    public ItemFactory setMaterial(@NotNull Material material) {
        ItemMeta meta = Bukkit.getItemFactory().asMetaFor(this.meta, material);
        if (meta == null) meta = Bukkit.getItemFactory().getItemMeta(material);
        this.material = material;
        this.meta = meta;
        return this;
    }

    public @NotNull Object getNMSItemStack() {
        ItemStack stack = create();
        String ver = Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];
        return Mirror.mirror("org.bukkit.craftbukkit." + ver + ".inventory.CraftItemStack")
                .method("asNMSCopy", ItemStack.class)
                .invoke(stack);
    }

    public @NotNull String getNBTString() {
        return serialise(create());
    }

    /**
     * @return The meta, with any given consumers applied
     */
    public <M extends ItemMeta> @NotNull M getItemMeta() {
        ItemMeta meta = this.meta.clone();
        try {
            for (Consumer<ItemMeta> consumer : consumers) {
                consumer.accept(meta);
            }
        } catch (Throwable ignore) {
        }
        return (M) meta;
    }

    public static ItemStack parse(String nbt, String converterMethod) {
        Object compound = NBT_PARSER.invoke(nbt);
        final Mirror<?> mirror = new Mirror<>(ITEM_CLASS);
        final MethodMirror<?> creator = mirror.method(converterMethod, NBT_CLASS);
        Object item = creator.invoke(compound);
        return ITEM_CONVERTER.invoke(item);
    }

    public static ItemStack parse(String nbt) {
        Object compound = NBT_PARSER.invoke(nbt);
        Object item = ITEM_CREATOR.invoke(compound);
        return ITEM_CONVERTER.invoke(item);
    }

    public static String serialise(ItemStack item) {
        Object nbt = new Mirror<>(NBT_CLASS).instantiate();
        Object itemStack = new Mirror<>(CRAFT_ITEM_CLASS)
                .method("asNMSCopy", ItemStack.class)
                .invoke(item);
        return new Mirror<>(itemStack)
                .method("save", NBT_CLASS)
                .invoke(nbt)
                .toString();
    }

    /**
     * This creates a new item given the provided material and amounts.
     * Any consumers will then be applied to its meta, and the meta will be
     * applied to the item.
     * <p>
     * Where possible, any invalid meta edits will be skipped to preserve
     * the process.
     *
     * @return The newly-created item
     */
    public @NotNull ItemStack create() {
        ItemStack itemStack = new ItemStack(material);
        itemStack.setAmount(Math.max(0, Math.min(127, amount)));
        ItemMeta meta = this.meta.clone();
        for (Consumer<ItemMeta> consumer : consumers) {
            try {
                consumer.accept(meta);
            } catch (Throwable ignore) {
            }
        }
        try {
            itemStack.setItemMeta(meta);
        } catch (Throwable ignore) {
        }
        for (Consumer<ItemStack> edit : itemEdits) {
            try {
                edit.accept(itemStack);
            } catch (Throwable ignore) {
            }
        }
        return itemStack;
    }

    public static abstract class FactoryImplementation {

        private ItemFactory factory;

        public FactoryImplementation() {
        }

        public abstract ItemFactory apply(ItemFactory factory);

        <Q extends FactoryImplementation> Q setTarget(ItemFactory factory) {
            this.factory = factory;
            return (Q) this;
        }

        public ItemFactory getFactory() {
            return apply(factory);
        }
    }

}
